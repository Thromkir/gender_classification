#!/bin/bash

python ./classification.py -m $1 -s "$2" -l ${3-./stuff/le.pickle} -t ${4-./stuff/tokenizer.pickle}
