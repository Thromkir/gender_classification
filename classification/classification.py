import numpy as np
from keras.models import load_model
import argparse
import pickle
from keras.preprocessing.sequence import pad_sequences

ap = argparse.ArgumentParser()
ap.add_argument("-m", "--model", type=str, required=True)
ap.add_argument("-s", "--string", type=str, required=True)
ap.add_argument("-l", "--le", type=str, required=True)
ap.add_argument("-t", "--tokenizer", type=str, required=True)
args = vars(ap.parse_args())
# loading model and stuff for classification
model = load_model(args["model"])
le = pickle.loads(open(args["le"], "rb").read())
tokenizer = pickle.loads(open(args["tokenizer"], "rb").read())
# preparing string for classification
MAX_SEQ_LENGTH = 256
message = args["string"]
message = tokenizer.texts_to_sequences([message])
message = pad_sequences(message, maxlen=MAX_SEQ_LENGTH)
# classification
result = np.argmax(model.predict(message), axis=1)
result = le.inverse_transform(result)[0]
print(result)
