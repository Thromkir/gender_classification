# gender_classification

========INFO========

Tested logistic regression, perceptron, convolution networks during the test task model creation. But their max accuracy reached only 68% on test data, so final model based on recurrent neural network (76% accuracy on test data). Also tested Global Vectors for Word Representation (GloVe https://nlp.stanford.edu/projects/glove/) for embedding layer creation, but it showed low efficency for the dataset. So in final model used embedding layer, which was trained along with neural network.
evaluation_result folder contains logs for tensorboard, final trained model and plot with accuracy and loss data during training and testing. After 15 epochs detected overfitting on training data, so final model is model with weights on 15 epoch.
Command to look on tensorboard logs: tensorboard --logdir=path/to/log-directory

========INSTALL========

To install dependencies for training and classification run install_dependencies.sh from your preffered virtual enviroment.
It will install python3-tk for plot creating library (matplotlib). Other libraries in the project:
- scipy, numpy - dependencies of scikit-learn and libraries for matrix operations;
- tensorflow - library for deep learning;
- keras - high level library for simple network architecture creation;
- scikit-learn - library for machine learning main algorithms, used for spliting data on training and testing parts, creating label encoder;
- tensorboard - for visualisation of the training process.

========TRAINING========

Training script is in training folder. nlp_train.py is main python script for model training and lstm.py in nets folder is neural network architecture, used in that task. Script usage:

./train.sh [train_data] [model_file] {eval_data} {intermediate_res} {stuff}

- train_data - path to the folder with training data;
- model_file - model file name and path for it;
- eval_data - path to the folder for saving tensorboard logs and plot with training/test loss and accuracy during model fitting;
- intermediate_res - path to the folder for saving models with best test accuracy during training and model on last epoch;
- stuff - path to the folder to save label encoder file and tokenizer for further classification.

By default eval_data, intermediate_res and stuff are set in folders on the same level as nlp_train.py

Example:
./train.sh ./gender_training_data ./res.hdf5 ./eval_data ./intermediate_res ./stuff

========CLASSIFICATION========

Classification script is in classification folder. classification.py is main python script. Script usage:

./classify.sh [model_file] [string] {label_encoder} {tokenizer}

- model_file - model to use in classifiction;
- string - string to classificate;
- label_encoder - path to the label encoder file, which was created during training;
- tokenizer - path to the tokenizer file, which was created during training;

Example:
./classify.sh ./gender_model.hdf5 "Test string" ./stuff/le.pickle ./stuff/tokenizer.pickle

Final model, label encoder and tokenizer from my network training by default is in classificate folder.
