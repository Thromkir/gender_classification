#!/bin/bash

python ./nlp_train.py -td $1 -mf $2 -ed ${3-./eval_data} -ir ${4-./intermediate_res} -s ${5-./stuff}
