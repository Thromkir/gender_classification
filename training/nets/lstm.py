from keras.models import Model
from keras.layers import Embedding, Input, Dense
from keras.layers import Bidirectional, LSTM


def createEmbeddingLayer(wordIndex, maxSeqLength):
    EMBEDDING_DIM = 100

    embeddingLayer = Embedding(
        len(wordIndex) + 1,
        EMBEDDING_DIM,
        input_length=maxSeqLength
    )
    return embeddingLayer


class RNN:
    @staticmethod
    def build(classes, wordIndex, maxSeqLength):
        sequenceInput = Input(shape=(maxSeqLength,), dtype="int32")
        embeddedSequences = createEmbeddingLayer(wordIndex=wordIndex, maxSeqLength=maxSeqLength)(sequenceInput)
        #################
        llstm = Bidirectional(LSTM(100))(embeddedSequences)
        preds = Dense(classes, activation="softmax")(llstm)
        #########################

        model = Model(sequenceInput, preds)
        return model
