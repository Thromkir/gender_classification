import matplotlib
import matplotlib.pyplot as plt
import os
import pickle
import numpy as np
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import train_test_split
from keras.utils import np_utils
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from nets.lstm import RNN
from keras.optimizers import Adamax
from keras.callbacks import ModelCheckpoint, TensorBoard
import argparse
from shutil import copyfile
matplotlib.use("Agg")

# getting script arguments
ap = argparse.ArgumentParser()
ap.add_argument("-td", "--training_data", type=str, required=True)
ap.add_argument("-mf", "--model_file", type=str, required=True)
ap.add_argument("-ed", "--eval_data", type=str, required=True)
ap.add_argument("-ir", "--intermediate_res", type=str, required=True)
ap.add_argument("-s", "--stuff", type=str, required=True)
args = vars(ap.parse_args())
# init training parameters
EPOCHS = 20
INIT_LR = 1e-3
BS = 64
# loading training data
allMessages = []
allLabels = []
with open(os.path.join(args["training_data"], "female.txt"), mode="r") as f:
    for line in f:
        line = line.replace("\n", "")
        allMessages.append(line)
        allLabels.append("female")

with open(os.path.join(args["training_data"], "male.txt"), mode="r") as f:
    for line in f:
        line = line.replace("\n", "")
        allMessages.append(line)
        allLabels.append("male")
# preparing training data
allLabels = np.array(allLabels)
le = LabelEncoder()
allLabels = le.fit_transform(allLabels)
allLabels = np_utils.to_categorical(allLabels, 2)
MAX_N_WORDS = 5000
MAX_SEQ_LENGTH = 256
tokenizer = Tokenizer(nb_words=MAX_N_WORDS)
tokenizer.fit_on_texts(allMessages)
sequences = tokenizer.texts_to_sequences(allMessages)
wordIndex = tokenizer.word_index
allMessages = pad_sequences(sequences, maxlen=MAX_SEQ_LENGTH)
(trainData, testData, trainLabels, testLabels) = train_test_split(allMessages,
                                                                  allLabels,
                                                                  test_size=0.25,
                                                                  random_state=42)
# init model
model = RNN.build(classes=len(le.classes_), wordIndex=wordIndex, maxSeqLength=MAX_SEQ_LENGTH)
opt = Adamax(lr=INIT_LR)
model.compile(loss="binary_crossentropy",
              optimizer=opt,
              metrics=["accuracy"])
callbacksList = [
    ModelCheckpoint(os.path.join(args["intermediate_res"], "weights.{epoch:02d}-{val_acc:.3f}acc-{val_loss:.2f}loss.hdf5"),
                    monitor="val_acc", verbose=0, save_best_only=True, save_weights_only=False, mode="max"),
    TensorBoard(log_dir=args["eval_data"], histogram_freq=5, batch_size=BS,
                write_graph=False, write_grads=False, update_freq="epoch")
]
# save stuff for further classification
f = open(os.path.join(args["stuff"], "le.pickle"), "wb")
f.write(pickle.dumps(le))
f.close()
f = open(os.path.join(args["stuff"], "tokenizer.pickle"), "wb")
f.write(pickle.dumps(tokenizer))
f.close()
# model training and saving
H = model.fit(trainData, trainLabels,
              validation_data=(testData, testLabels),
              epochs=EPOCHS, verbose=1, batch_size=BS, callbacks=callbacksList)

bestModelFilename = sorted(os.listdir(args["intermediate_res"]), reverse=True)[0]
model.save(os.path.join(args["intermediate_res"], "last_gender_classificator.net"))
copyfile(os.path.join(args["intermediate_res"], bestModelFilename), args["model_file"])
# create and save plot
plt.style.use("ggplot")
plt.figure()
N = EPOCHS

plt.plot(np.arange(0, N), H.history["loss"], label="train_loss")
plt.plot(np.arange(0, N), H.history["val_loss"], label="val_loss")
plt.plot(np.arange(0, N), H.history["acc"], label="train_acc")
plt.plot(np.arange(0, N), H.history["val_acc"], label="val_acc")
plt.title("Training Loss and Accuracy")
plt.xlabel("Epoch #")
plt.ylabel("Loss/Accuracy")
plt.legend(loc="upper left")
plt.savefig(os.path.join(args["eval_data"], "plot.png"))
